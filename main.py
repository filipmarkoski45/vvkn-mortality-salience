import numpy as np
import pandas as pd
from statsmodels.stats.weightstats import ztest
from tabulate import tabulate

# Local Imports
from definitions import timer, cases_w1, cases_wo1
from preprocess_data import load_data


def analyse(ctrl, exp, facet: str = 'answers'):
    features: list = getattr(ctrl, facet).columns.to_list()
    [features.remove(feature) for feature in ['participant', 'TIME_start', 'TIME_end', 'TIME_total']]

    print(getattr(ctrl, facet).shape)
    print(getattr(exp, facet).shape)

    if facet == 'answers':
        cases = cases_w1
    elif facet == 'times':
        cases = cases_wo1
    else:
        raise ValueError("The `facet` parameter only supports 'answers' and 'times'")

    results = pd.DataFrame()
    for feature in cases:

        if facet == 'answers':
            feature_1 = feature
            feature = feature_1[:-2]

            ctrl.answers.rename(columns={feature_1: feature}, inplace=True)
            exp.answers.rename(columns={feature_1: feature}, inplace=True)

        control = getattr(ctrl, facet)[feature]
        experiment = getattr(exp, facet)[feature]

        print(f'For {feature}:')

        # The link below provides the reason why p / 2
        # https://stackoverflow.com/a/15984310/3950168
        statistic, p_value = ztest(x1=control, x2=experiment, alternative='smaller')
        print(f'\tStatistic: {statistic:.3}')
        print(f'\tp-value: {p_value/2:.3}')

        results.loc[feature, 'ctrl_mean'] = control.mean()
        results.loc[feature, 'exp_mean'] = experiment.mean()
        results.loc[feature, 'ctrl_std'] = control.std()
        results.loc[feature, 'exp_std'] = experiment.std()
        results.loc[feature, 'ctrl_mode'] = control.mode()[0]
        results.loc[feature, 'exp_mode'] = experiment.mode()[0]
        results.loc[feature, 'mean: exp > ctrl'] = bool(experiment.mean() > control.mean())
        results.loc[feature, 'mode: exp > ctrl'] = bool(experiment.mode()[0] > control.mode()[0])
        results.loc[feature, 'mean: exp < ctrl'] = bool(experiment.mean() < control.mean())
        results.loc[feature, 'mode: exp < ctrl'] = bool(experiment.mode()[0] < control.mode()[0])

    # with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    #    print(results)
    print(tabulate(results, headers='keys', tablefmt='psql'))


def average_mean_answers():
    ctrl = np.array([3.5, 4.575, 3.575, 2.85, 2.5, 4.175])
    exp = np.array([3.25714, 4.08571, 3.25714, 2.57143, 2.2, 4.14286])

    print(ctrl.mean())
    print(exp.mean())


def average_mean_times():
    ctrl = np.array([22623.6, 31364.8, 28585.7, 22735.2, 19673.2, 25088.4])
    exp = np.array([23556.0, 32109.0, 30564.7, 25122.5, 27772.7, 57761.0])

    print(ctrl.mean())
    print(exp.mean())


@timer
def main():
    ctrl, exp = load_data()

    print()
    print(tabulate(['Survey Answers'], tablefmt='rst'))
    analyse(ctrl=ctrl, exp=exp, facet='answers')
    average_mean_answers()

    print()
    print(tabulate(['Survey Response Times'], tablefmt='rst'))
    analyse(ctrl=ctrl, exp=exp, facet='times')
    average_mean_times()


if __name__ == '__main__':
    main()
