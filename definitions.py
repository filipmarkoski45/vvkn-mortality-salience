import datetime
import os

# pip install ipykernel && python -m ipykernel install --user --name=venv

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
DATA_DIR = os.path.join(ROOT_DIR, 'data')
DOCS_DIR = os.path.join(ROOT_DIR, 'docs')
TESTS_DIR = os.path.join(ROOT_DIR, 'tests')
PLOTS_DIR = os.path.join(ROOT_DIR, 'plots')
SERIALIZED_DIR = os.path.join(ROOT_DIR, 'serialized')
MODELS_DIR = os.path.join(SERIALIZED_DIR, 'models')
RNG_SEED = 1

training_set_file_path = f'{DATA_DIR}/train.csv'
testing_set_file_path = f'{DATA_DIR}/test.csv'

# Defining the file paths for each .csv
control_group_data_path = f'{DATA_DIR}/no_death_salience_data.csv'
control_group_data_times_path = f'{DATA_DIR}/no_death_salience_data_times.csv'

experimental_group_data_path = f'{DATA_DIR}/death_salience_data.csv'
experimental_group_data_times_path = f'{DATA_DIR}/death_salience_data_times.csv'

cases_w1 = [
    'case_scholarship_embezzlement:1',
    'case_didnt_stop:1',
    'case_permanent_hoarseness:1',
    'case_judicial_misconduct:1',
    'case_indecent_exposure:1',
    'case_fake_doctor:1',
]

cases_wo1 = [
    'case_scholarship_embezzlement',
    'case_didnt_stop',
    'case_permanent_hoarseness',
    'case_judicial_misconduct',
    'case_indecent_exposure',
    'case_fake_doctor',
]


def timer(func):
    def f(*args, **kwargs):
        start = datetime.datetime.now()
        timestamp = '{date:%Y-%m-%d %H:%M:%S}'.format(date=start)
        print(f'Execution started: {timestamp}')
        rv = func(*args, **kwargs)
        end = datetime.datetime.now()
        timestamp = '{date:%Y-%m-%d %H:%M:%S}'.format(date=end)
        print(os.linesep)
        print(f'Execution ended: {timestamp}')
        print('Execution elapsed: ', end - start)
        return rv

    return f
