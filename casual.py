import sys

from tabulate import tabulate

from definitions import timer
from preprocess_data import load_data


@timer
def main():
    if sys.prefix:
        print('Executed from a virtual environment')

    print(tabulate(['Control Group'], tablefmt='rst'))

    ctrl, exp = load_data()

    if hasattr(ctrl, 'answers'):
        print(ctrl.answers.shape)

    print(getattr(ctrl, 'answers').shape)

    features: list = ctrl.answers.columns.to_list()
    try:
        features.remove('none')
    except ValueError:
        pass




if __name__ == '__main__':
    main()
