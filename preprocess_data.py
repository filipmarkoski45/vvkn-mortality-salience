from collections import namedtuple

import pandas as pd

from definitions import control_group_data_path, control_group_data_times_path
from definitions import experimental_group_data_path, experimental_group_data_times_path
# Local Imports
from pd_utils import missing_data_ratio


def create_namedtuple(answers: object, times: object) -> object:
    t = namedtuple('Group', ['answers', 'times'])
    t.answers = answers
    t.times = times
    return t


def read_csv(path: str):
    return pd.read_csv(path, low_memory=False, encoding='utf-8', index_col=False)


def drop_unfinished_surveys(df: pd.DataFrame, feature='TIME_end', inplace=True):
    selection = df[df[feature].isna()].index

    if inplace:
        return df.drop(selection, inplace=True)
    return df.drop(selection)


def fill_col_na_with_mode(df: pd.DataFrame, feature='age:1') -> pd.DataFrame:
    most_frequent_age = df[feature].mode()[0]
    df[feature] = df[feature].fillna(value=most_frequent_age)
    return df


def preprocess_outliers(ctrl):
    df: pd.DataFrame = ctrl.answers

    # Age Outliers: 4
    feature = 'age:1'
    outliers = [4]
    age_mode = df[feature].mode()[0]
    df.loc[outliers, feature] = age_mode

    # Education Outliers: 4, 17
    feature = 'education:1'
    outliers = [4, 17]
    education_mode = df[feature].mode()[0]
    df.loc[outliers, feature] = education_mode

    return df


def load_data():
    # Reading each of the files with extension .csv
    ctrl = read_csv(control_group_data_path)
    ctrl_times = read_csv(control_group_data_times_path)

    exp = read_csv(experimental_group_data_path)
    exp_times = read_csv(experimental_group_data_times_path)

    # Dropping the unfinished surveys from each of the data sets
    drop_unfinished_surveys(ctrl, inplace=True)
    drop_unfinished_surveys(exp, inplace=True)

    # Filling in the age column with the age mode
    ctrl = fill_col_na_with_mode(df=ctrl, feature='age:1')
    exp = fill_col_na_with_mode(df=exp, feature='age:1')

    # Storing the ids for each of the proper surveys
    id_feature = 'participant'

    ctrl_ids = ctrl[id_feature].copy()
    exp_ids = exp[id_feature].copy()

    # Using the ids of the proper surveys we get their response times
    ctrl_times = ctrl_times.loc[ctrl_times[id_feature].isin(ctrl_ids)].copy()
    exp_times = exp_times.loc[exp_times[id_feature].isin(exp_ids)].copy()

    # Creating named tuples to join the answers and the response times
    ctrl = create_namedtuple(answers=ctrl, times=ctrl_times)
    exp = create_namedtuple(answers=exp, times=exp_times)

    # Pre-process the outliers
    ctrl.answers = preprocess_outliers(ctrl)

    return ctrl, exp


def main():
    ctrl, exp = load_data()

    print(missing_data_ratio(ctrl.answers))
    print(missing_data_ratio(exp.answers))
    print(missing_data_ratio(ctrl.times))
    print(missing_data_ratio(exp.times))


if __name__ == '__main__':
    main()
